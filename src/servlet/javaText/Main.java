package servlet.javaText;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main extends Thread {

    public static void main(String[] args) {
        //prints 10 values for the current thread(s).
        for(int i=0; i<100; i++) {
            System.out.println("Thread: " + Thread.currentThread().getId() + " value " + i);

			/*
			This will allow the code to print the multiple threads for each new object
			from  the JavaThreads method below.
			*/
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

}

class JavaThreads {

    //created  objects of the Threads method and starts them.
    public static void main(String[] args) {
        Threads threads = new Threads();
        threads.start();
        Threads threads2 = new Threads();
        threads2.start();
        Threads threads3 = new Threads();
        threads3.start();
    }

    private static class Threads {
        public void start() {
        }
    }
}





class JavaRunnable implements Runnable {

    public void run() {

        //prints 100 values for the current thread(s).
        for (int i = 0; i < 100; i++) {
            System.out.println("This is process: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}

class JavaExecutors {

    public static void main (String[] args) throws InterruptedException {

        //Uses the ExecutorService to limit how many threads can run concurrently
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        //Counter is set to 10
        for(int i=0; i<=100; i++) {

            //call executorService variable and executes a new runnable command.
            executorService.execute(new Runnable() {

                public void run() {
                    //Mimics a series of tasks that need to processed.  Uses Try/Catch for error handling.
                    try {
                        System.out.println("Task 1");
                        Thread.currentThread();
                        Thread.sleep(1500);
                        System.out.println("Task 2");
                        Thread.currentThread();
                        Thread.sleep(1500);
                        System.out.println("Task 3");
                        Thread.currentThread();
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {

                        e.printStackTrace();
                    }

                }

            });

        }

    }

}

 class JavaRunnableProcessTest {

    public static void main(String[] args) {

        //Calls the JavaRunnable class, creates a new object, and assigned it to the thread variable.
        Thread thread = new Thread(new JavaRunnable());
        thread.start();

    }

}




